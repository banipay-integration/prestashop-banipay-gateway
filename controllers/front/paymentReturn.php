<?php
class BanipayPaymentReturnModuleFrontController extends  ModuleFrontController
{

    public function initContent()
    {
        parent::initContent();
        $order_id = Tools::getValue('order_id');
        $status = Tools::getValue('status');
        if($status == 'DECLINED' && $order_id) {
            $objOrder = new Order($order_id); //order with id=1
            $history = new OrderHistory();
            $history->id_order = (int)$objOrder->id;
            $history->changeIdOrderState(8, (int)($objOrder->id)); //order status=3
        }
        $this->context->smarty->assign('status', $status);
        $this->context->smarty->assign('order_id', $order_id);
        $this->setTemplate('module:banipay/views/template/front/paymentError.tpl');
    }

}
