{*
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
*}

{extends file='page.tpl'}
{block name="content"}
    <section id="content-payment-return" class="card definition-list">
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">
                    {if $status == 'DECLINED'}
                        <p>
                            Ocurrio un error con el pago, Intente nuevamente.
                        </p>
                    {/if}
                    <p>
                        Banipay, Medios de Pago
                    </p>
                </div>
            </div>
        </div>
    </section>
{/block}
